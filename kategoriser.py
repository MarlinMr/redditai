from transformers import pipeline
import praw, json, time

with open("redditConfig.json", 'r',encoding='utf8') as f:
    data = json.load(f)
    reddit = praw.Reddit(client_id=data["client_id"],
        client_secret=data["client_secret"],
        user_agent=data["user_agent"],
        username=data["username"],
        password=data["password"])
    sub=data["sub"]

classifier = pipeline("zero-shot-classification", model="NbAiLab/nb-bert-base-mnli")
candidate_labels = ['rus', 'ulovlig', 'politikk', 'helse', 'sport', 'religion', 'krig', 'nyheter', 'hjelp', 'humor', 'kultur']
hypothesis_template = 'Dette eksempelet er {}.'

subreddit = reddit.subreddit(sub)

for comment in subreddit.stream.comments(skip_existing=True):
    result = classifier(comment.body, candidate_labels, hypothesis_template=hypothesis_template, multi_class=True)
    print("\n" + time.ctime(int(comment.created_utc))+ " UTC - Author: /u/" + str(comment.author) + " - Link: https://old.reddit.com" + comment.permalink)
    print("\n" + str(result["sequence"][:255]))
    print("\n" + str(result["labels"]))
    print(result["scores"])
    for i in range(0, len(str(result["scores"]))):
        print("-", end='')
    print("\n")
