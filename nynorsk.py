from transformers import pipeline
import praw, json, time

with open("redditConfig.json", 'r',encoding='utf8') as f:
    data = json.load(f)
    reddit = praw.Reddit(client_id=data["client_id"],
        client_secret=data["client_secret"],
        user_agent=data["user_agent"],
        username=data["username"],
        password=data["password"])
    sub=data["sub"]

translator = pipeline("translation", model='pere/nb-nn-translation')

subreddit = reddit.subreddit(sub)

for comment in subreddit.stream.comments(skip_existing=True):
    print("\n" + time.ctime(int(comment.created_utc))+ " UTC - Author: /u/" + str(comment.author) + " - Link: https://old.reddit.com" + comment.permalink)
    print("Original: " + comment.body[:255].lstrip())
    print("Oversatt:", translator(comment.body, max_length=255)[0]["translation_text"]+ "\n\n")
